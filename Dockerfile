

#FROM ubuntu


#RUN apt-get update \
 # && apt-get install -y python3-pip python3-dev \
 # && cd /usr/local/bin \
 # && ln -s /usr/bin/python3 python \
 # && pip3 --no-cache-dir install --upgrade pip \
 # && rm -rf /var/lib/apt/lists/*

#RUN apt-get update -y && \
#    apt-get install -y python3
#RUN apt-get install -y python-pip

FROM ubuntu:20.04
RUN apt-get update && apt-get upgrade -y && apt-get install -y python3.8 python3.8-dev && apt-get install -y python3-pip

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "python" ]

CMD [app.py]